# How to configure Eyes to interact with GitLab    
Integrating GitLab into Eyes is done using the Test Manager and must be done by a user with Eyes team administration privileges. This integration involves the following steps:
- Use the Page navigator to display the Admin page. You will only see this menu entry if you have Admin privileges.
- Select the Teams tile.
- Click on the row of the team you want to configure.
- Click on the Integrations tab.
- Find the GitLab section and click on it.
- Select your server from the drop-down list. (gitlab.com)
- If your server is not on the list, then click the Add button and enter the URL in the dialog that opens.
- Click the Manage repositories button.
- If you have not already logged into GitLab, you will see a message asking you to do so. Click the link and enter your GitLab credentials.
- Once you have logged into GitLab, you can see a list of repositories associated with the logged-in GitLab account.
- The dialog includes a filter you can use to narrow down the list of repositories.
- Select the repositories that you require and click Apply.
- A list of selected repositories is now visible. You can remove a repository from the list by hovering over it in the list and clicking the  button.

If your organization uses an Eyes on-premise server, in addition to the above, you should also white list inbound HTTPS traffic from the GitLab server to your Eyes server in your organization's firewall.
 
# How to configure GitLab to run Eyes tests
In the GitLab configuration file .gitlab-ci.yml, add the following lines:

```
variables:
	APPLITOOLS_API_KEY: "YourAPiKey" 
	APPLITOOLS_BATCH_ID: ${CI_COMMIT_SHA}
```


- ${CI_COMMIT_SHA} Is the default environment variable for GitLab commit sha, this is used with GitLab’s CI/CD.
- Replace the string YourAPiKey with your API key. To get the value of your API key, see How to obtain your API key. 

# Add a CI/CD variable to a project
You can add CI/CD variables to a project’s settings. Only project members with the Maintainer role can add or update project CI/CD variables. To keep a CI/CD variable secret, put it in the project settings, not in the .gitlab-ci.yml file.
To add or update variables in the project settings:
- Go to your project’s Settings > CI/CD and expand the Variables section.
- Select the Add Variable button and fill in the details:
- Key: Must be one line, with no spaces, using only letters, numbers, or _.
- Value: No limitations.
- Type: File or Variable.
- Environment scope: Optional. All, or specific environments.
- Protect variable Optional. If selected, the variable is only available in pipelines that run on protected branches or tags.
- Mask variable Optional. If selected, the variable’s Value is masked in job logs. The variable fails to save if the value does not meet the masking requirements.
 
You then can add your APPLITOOLS_API_KEY as an environment variable. 
In the GitLab configuration file:

```
Variables:
	APPLITOOLS_API_KEY: $APPLITOOLS_API_KEY 
	APPLITOOLS_BATCH_ID: ${CI_COMMIT_SHA}
```

 
# GitLab CI Configuration File “.gitlab.-ci.yml”
To use GitLab CI/CD, you need:
Application code hosted in a Git repository.
A file called .gitlab-ci.yml in the root of your repository, which contains the CI/CD configuration.
In the .gitlab-ci.yml file, you can define:
The scripts you want to run.
Other configuration files and templates you want to include.
Dependencies and caches.
The commands you want to run in sequence and those you want to run in parallel.
The location to deploy your application to.
Whether you want to run the scripts automatically or trigger any of them manually.
The scripts are grouped into jobs, and jobs run as part of a larger pipeline. You can group multiple independent jobs into stages that run in a defined order.
When you add a .gitlab-ci.yml file to your repository, GitLab detects it and an application called GitLab Runner runs the scripts defined in the jobs.

Example:

```
image: markhobson/maven-chrome:jdk-11

variables:
  APPLITOOLS_API_KEY: $APPLITOOLS_API_KEY
  APPLITOOLS_BATCH_ID: ${CI_COMMIT_SHA}

test-job1:
  stage: test
  script:
    - echo "Starting a test job"
    - echo $APPLITOOLS_API_KEY
    - echo ${CI_COMMIT_SHA}
    - mvn clean
    - mvn test
```



# Working with GitLab
Once your system is configured as described above, you can use GitLab in the usual way. To make sure that the environment variables are properly set you can print them to the console output by adding the following script commands to that job:
```
    - echo $APPLITOOLS_API_KEY
    - echo ${CI_COMMIT_SHA}
```

A GitLab pipeline is run when changes are pushed to a branch in your repository. This pipeline will typically include an Eyes visual test.
The Eyes GitLab integration is based on the GitLab merge request. When there is an open merge request, Eyes adds the following functionality to your GitLab workflow:
When GitLab runs a pipeline that includes an Eyes test, Eyes detects that the run is associated with a GitLab merge request and searches for a baseline to use as a reference. Eyes checks for a matching baseline in the following order:
On a branch that corresponds to the repository name and source branch defined by the merge request.
If there is no such branch, it creates a branch and populates it with the latest baseline for that test from the repository name and target branch defined by the merge request.
If there is no such baseline, the test will be marked as New.
Eyes sends the GitLab server status information and this is displayed in the GitLab CI results.

### Initiating the merge
When you click on the scm/applitools link, the browser switches to the Test Manager Merge branches page reloaded with the source and target branches derived from the merge request. 

